class Animal(val name: String)

class Zoo(val animals: List<Animal>) {
    operator fun iterator(): Iterator<Animal> = animals.iterator()
}


fun main () {
    val idols = listOf("Pekora", "Miko", "Rusia")
    for (idol in idols) println("Love $idol")

    var countIdols = 0
    while (countIdols < 5) {
        println("Love Idol")
        countIdols++
    }
    do {
        println("Idol Love")
        countIdols--
    } while (countIdols > 0)

    val zoo = Zoo(listOf(Animal("zebra"), Animal("lion")))

    for (animal in zoo) println("Watch out, it's a ${animal.name}")

}