fun strLength(notNull: String): Int {                   // 7
    return notNull.length
}

fun main () {
    var neverNull: String = "Can't null"
//    neverNull = null

    var nullable: String? = "Can null"
    nullable = null

//    var inferredNonNull = "assume non-null"
//    inferredNonNull = null

    strLength(neverNull)
//    strLength(nullable)
}