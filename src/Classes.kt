class Customer

class Contact(val id: Int, var email: String)

fun main () {
    val customer = Customer()
    val contact = Contact(1, "supakin@gmail.com")

    println(contact.id)
    contact.email = "sakunwa@gmail.com"
    println(contact.email)
}