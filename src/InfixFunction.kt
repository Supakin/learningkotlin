class Person (val name: String) {
    val likedPeople = mutableListOf<Person>()
    infix fun likes(other: Person) { likedPeople.add(other) }
    fun showLikedPeople () {
        likedPeople.forEach { print("${it.name} ") }
    }

}

fun main () {
    infix fun Int.times (str: String) = str.repeat(this)
    println(2 times "Bye")

    val pair = "Ferrari" to "Katrina"
    println(pair)

    infix fun String.onto (other: String) = Pair(this, other)
    val myPair = "Pekora" onto "Miko"
    println(myPair)

    val Pekora = Person("Pekora")
    val Miko = Person("Miko")
    Pekora likes Miko
    Pekora likes Person("Watame")
    Pekora.showLikedPeople()
}

