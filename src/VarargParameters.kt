fun showAll (vararg messages: String) {
    for (m in messages) println("$m ")
}

fun showMessageWithPrefix (vararg messages: String, prefix: String) {
    for (m in messages) println(prefix + m)
}


fun main () {
    showAll("Pekora", "Miko", "Rusia")
    showMessageWithPrefix(
        "Pekora", "Miko",
        prefix = "Hello, "
    )
}
