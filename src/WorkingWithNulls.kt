fun isMessage (message: String? = null): String {
    if (message != null && message.length > 0) return "This is message"
    return "empty message or null"
}

fun main () {
    println(isMessage("Pekora so cute"))
    println(isMessage(""))
    println(isMessage(null))
}