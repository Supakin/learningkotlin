fun showMessage (message: String): Unit {
    println(message)
}

fun showMessageWithPrefix (message: String, prefix: String = "Info") {
    println("[$prefix] $message")
}

fun sum (x: Int, y: Int): Int {
    return x + y
}

fun multiply (x: Int, y: Int) = x * y

fun main () {
    showMessage("Hello, Pekora")
    showMessageWithPrefix("Pekora")
    println(sum(4,5))
}