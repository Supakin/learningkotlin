fun main () {
    operator fun Int.times (str: String) = str.repeat(this)
    println(2 * "Bye ")

    operator fun String.get(range: IntRange) = substring(range)
    val str = "Honkai Impact 3"
    println(str[0..2])


}